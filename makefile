

CC = gcc
CPPFLAGS = -I . -I ..
CPPFLAGS += -D DEBUG=1
CFLAGS += -Wall -Werror=implicit-function-declaration
# LDLIBS = -lm

TARGET = libprintf

default: $(TARGET).a

srclist.mk:
	find -name '*.c' ! -path './#*' | sed 's/^/SRCS += /' > srclist.mk

include srclist.mk

OBJS = $(SRCS:.c=.o)
DEPENDS = $(SRCS:.c=.mk)

install: ../$(TARGET).a ../$(TARGET).h

run: $(TARGET)
	./$(TARGET) $(ARGS)

valrun: $(TARGET)
	valgrind ./$(TARGET) $(ARGS)

../$(TARGET).a: $(TARGET).a
	cp $(TARGET).a ../

../$(TARGET).h: $(TARGET).h
	cp $(TARGET).h ../

target_h_deps += vgprintf.h

$(TARGET).h: $(target_h_deps)
	echo "#ifndef" $(TARGET)"_H" > $(TARGET).h
	echo "#define" $(TARGET)"_H" >> $(TARGET).h
	cat $(target_h_deps) >> $(TARGET).h
	echo "#endif" >> $(TARGET).h

$(TARGET).a: $(OBJS)
	$(LD) -r $^ -o $@

%.l.mk: %.l
	echo "$(basename $<).c: $<" > $@

%.y.mk: %.y
	echo "$(basename $<).c: $<" > $@

%.mk: %.c
	$(CPP) -MM -MT $@ $(CPPFLAGS) -MF $@ $<

%.o: %.c %.mk
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $< -o $@

.PHONY: clean deep-clean

clean:
	rm -f $(OBJS) $(DEPENDS) srclist.mk
	rm -f $(TARGET).a
	rm -f $(TARGET).h

deep-clean:
	find -type f -regex '.*\.mk' -delete
	find -type f -regex '.*\.o' -delete
	find -type f -regex '.*\.a' -delete
	find -type f -executable -delete

include $(DEPENDS)

