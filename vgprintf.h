size_t libvgprintf
(
	void (*callback)(const char* src, size_t n),
	const char* fmt,
	va_list args
);
