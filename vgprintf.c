

#include <stdarg.h>
#include <string.h>

// most of this I copied from:
// https://github.com/torvalds/linux/blob/master/arch/x86/boot/printf.c

#include <libdebug.h>
#include <libchar.h>

#define ZEROPAD	1		/* pad with zero */
#define SIGN	2		/* unsigned/signed long */
#define PLUS	4		/* show plus */
#define SPACE	8		/* space if plus */
#define LEFT	16		/* left justified */
#define SMALL	32		/* Must be 32 == 0x20 */
#define SPECIAL	64		/* 0x */

static char space = ' ', percent = '%';

static int skip_atoi(const char **s)
{
	int i = 0;

	while (is_digit(**s))
		i = i * 10 + *((*s)++) - '0';
	return i;
}

static char *number
(
	char *str,
	long num,
	int base,
	int size,
	int precision,
	int type)
{
	/* we are called with base 8, 10 or 16, only, thus don't need "G..."  */
	static const char digits[] = "0123456789ABCDEF"; /* "GHIJKLMNOPQRSTUVWXYZ"; */

	char tmp[66];
	char c, sign, locase;
	int i;

	/* locase = 0 or 0x20. ORing digits or letters with 'locase'
	 * produces same digits or (maybe lowercased) letters */
	locase = (type & SMALL);
	if (type & LEFT)
		type &= ~ZEROPAD;
	if (base < 2 || base > 16)
		return NULL;
	c = (type & ZEROPAD) ? '0' : ' ';
	sign = 0;
	if (type & SIGN) {
		if (num < 0) {
			sign = '-';
			num = -num;
			size--;
		} else if (type & PLUS) {
			sign = '+';
			size--;
		} else if (type & SPACE) {
			sign = ' ';
			size--;
		}
	}
	if (type & SPECIAL) {
		if (base == 16)
			size -= 2;
		else if (base == 8)
			size--;
	}
	i = 0;
	if (num == 0)
		tmp[i++] = '0';
	else
		while (num != 0)
			tmp[i++] = (digits[__do_div(num, base)] | locase);
	if (i > precision)
		precision = i;
	size -= precision;
	if (!(type & (ZEROPAD + LEFT)))
		while (size-- > 0)
			*str++ = ' ';
	if (sign)
		*str++ = sign;
	if (type & SPECIAL) {
		if (base == 8)
			*str++ = '0';
		else if (base == 16) {
			*str++ = '0';
			*str++ = ('X' | locase);
		}
	}
	if (!(type & LEFT))
		while (size-- > 0)
			*str++ = c;
	while (i < precision--)
		*str++ = '0';
	while (i-- > 0)
		*str++ = tmp[i];
	while (size-- > 0)
		*str++ = ' ';
	return str;
}

size_t libvgprintf
(
	void (*callback)(const char* src, size_t n),
	const char* fmt,
	va_list args
)
{
	size_t ret = 0;
	ENTER;
	
	char* s;
	
	int len;
	int flags;
	int field_width;
	int precision;
	int base;
	int qualifier;
	unsigned long num;
	
	for(;*fmt;++fmt)
	{
		if(*fmt != '%')
		{
			callback(fmt, 1);
			continue;
		}
		flags = 0;

		repeat:
		++fmt;		/* this also skips first '%' */
		switch (*fmt)
		{
			case '-': flags |= LEFT;    goto repeat;
			case '+': flags |= PLUS;    goto repeat;
			case ' ': flags |= SPACE;   goto repeat;
			case '#': flags |= SPECIAL; goto repeat;
			case '0': flags |= ZEROPAD; goto repeat;
		}
		
		/* get field width */
		field_width = -1;
		if(is_digit(*fmt))
		{
			field_width = skip_atoi(&fmt);
		}
		else if(*fmt == '*')
		{
			++fmt;
			/* it's the next argument */
			field_width = va_arg(args, int);
			if(field_width < 0)
			{
				field_width = -field_width;
				flags |= LEFT;
			}
		}
		
		
		/* get the precision */
		precision = -1;
		if(*fmt == '.')
		{
			++fmt;
			if(is_digit(*fmt))
			{
				precision = skip_atoi(&fmt);
			}
			else if(*fmt == '*')
			{
				++fmt;
				/* it's the next argument */
				precision = va_arg(args, int);
			}
			if (precision < 0)
			{
				precision = 0;
			}
		}

		/* get the conversion qualifier */
		qualifier = -1;
		if (*fmt == 'h' || *fmt == 'l' || *fmt == 'L')
		{
			qualifier = *fmt;
			++fmt;
		}


		/* default base */
		base = 10;

		switch(*fmt)
		{
			case 'c':
			{
				if(!(flags & LEFT))
				{
					while(--field_width > 0)
					{
						callback(&space, 1);
					}
				}
				{
					char c = (unsigned char) va_arg(args, int);
					callback(&c, 1);
				}
				while(--field_width > 0)
				{
					callback(&space, 1);
				}
				continue;
			}
			
			case 's':
			{
				s = va_arg(args, char *);
				len = strnlen(s, precision);
				if(!(flags & LEFT))
				{
					while(len < field_width--)
					{
						callback(&space, 1);
					}
				}
				callback(s, len);
				while(len < field_width--)
				{
					callback(&space, 1);
				}
				continue;
			}
			
			case 'p':
			{
				if(field_width == -1)
				{
					field_width = 2 * sizeof(void *);
					flags |= ZEROPAD;
				}
				str = number(str,
					(unsigned long) va_arg(args, void *),
					16,
					field_width,
					precision,
					flags);
				continue;
			}
#if 0

		case 'n':
			if (qualifier == 'l') {
				long *ip = va_arg(args, long *);
				*ip = (str - buf);
			} else {
				int *ip = va_arg(args, int *);
				*ip = (str - buf);
			}
			continue;

		case '%':
			*str++ = '%';
			continue;

			/* integer number formats - set up the flags and "break" */
		case 'o':
			base = 8;
			break;

		case 'x':
			flags |= SMALL;
		case 'X':
			base = 16;
			break;

		case 'd':
		case 'i':
			flags |= SIGN;
		case 'u':
			break;
#endif

			default:
			{
				callback(&percent, 1);
				if(*fmt)
				{
					callback(fmt, 1);
				}
				else
				{
					--fmt;
				}
				continue;
			}
		}
		if(qualifier == 'l')
		{
			num = va_arg(args, unsigned long);
		}
		else if(qualifier == 'h')
		{
			num = (unsigned short) va_arg(args, int);
			if(flags & SIGN)
			{
				num = (short) num;
			}
		}
		else if(flags & SIGN)
		{
			num = va_arg(args, int);
		}
		else
		{
			num = va_arg(args, unsigned int);
		}
		#if 0
		str = number(str, num, base, field_width, precision, flags);
		#endif
		TODO;
	}
	
	EXIT;
	return ret;
}





















